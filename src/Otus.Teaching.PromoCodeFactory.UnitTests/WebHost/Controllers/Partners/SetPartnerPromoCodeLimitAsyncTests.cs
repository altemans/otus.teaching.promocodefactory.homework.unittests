﻿using AutoFixture;
using AutoFixture.AutoMoq;

using FluentAssertions;

using Microsoft.AspNetCore.Mvc;

using Moq;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly PartnersController _partnersController;

        Fixture _autoFixture;


        public Partner CreatePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(1),
                        Limit = 100
                    }
                }
            };
            return partner;
        }
        public Partner CreatePartnerWithCancelData()
        {
            var basPartner = CreatePartner();
            basPartner.PartnerLimits = new List<PartnerPromoCodeLimit>() {
                new PartnerPromoCodeLimit(){
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(1),
                    CancelDate = DateTime.Now,
                    Limit = 100
                }
            };
            return basPartner;
        }

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _autoFixture = new Fixture();
        }
        /// <summary>
        /// Not Found 404
        /// </summary>
        [Fact]
        public async void SetAsyncPartnerPromoCodeLimit_PartnerIsNotFound()
        {
            //Arange
            var partnerId = Guid.NewGuid();
            Partner partner = null;
            var request = _autoFixture.Create<SetPartnerPromoCodeLimitRequest>();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner); // если выполняем метод с параметром partnerId
                                                                                                // , то должно возвращать partner
                                                                                                // == null (partner не найден)

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }


        /// <summary>
        /// Bad request 400
        /// </summary>
        [Fact]
        public async void SetAsyncPartnerPromoCodeLimit_PartnerIsLocked()
        {
            //Arange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            Partner partner = CreatePartner();
            partner.IsActive = false;
            var request = _autoFixture.Create<SetPartnerPromoCodeLimitRequest>();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetAsyncPartnerPromoCodeLimit_HasActiveLimits()
        {
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            Partner partner = CreatePartner();
            var request = _autoFixture.Create<SetPartnerPromoCodeLimitRequest>();
            
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var limits = partner.PartnerLimits.ToList();

            //Assert
            Assert.True(limits[0].CancelDate.HasValue);
        }

        [Fact]
        public async void SetAsyncPartnerPromoCodeLimit_LimitIsNull()
        {
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            Partner partner = CreatePartner();
            var request = _autoFixture.Create<SetPartnerPromoCodeLimitRequest>();
            request.Limit = 0;
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

    }
}